<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Home</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="css/mdb.css" />
<link rel="stylesheet" type="text/css" href="css/compiled.min.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
</head>
<body>
<div class="container">
	<div class="card mt-3">
		<div class="card-body">
			<h4 class="card-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h4>
			<p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec pretium, enim at ultrices malesuada, purus orci tristique nisl, sit amet imperdiet odio quam vitae elit.</p>
			<a href="question.php" class="btn btn-primary">Start</a>
		</div>
	</div>
</div>
</body>
</html>