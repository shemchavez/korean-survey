<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Question</title>
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="css/mdb.css" />
<link rel="stylesheet" type="text/css" href="css/compiled.min.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
</head>
<body>
<div class="container">
    <div class="card mt-3">
        <div class="card-body">
            <ul id="tab" class="nav md-pills nav-justified pills-secondary">
                <li class="nav-item">
                    <a class="nav-link active show" data-toggle="tab" href="#panel1" role="tab">1</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#panel2" role="tab">2</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#panel3" role="tab">3</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#panel4" role="tab">4</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#panel5" role="tab">5</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#panel6" role="tab">6</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#panel7" role="tab">7</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#panel8" role="tab">8</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#panel9" role="tab">9</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#panel10" role="tab">10</a>
                </li>
            </ul>
            <!-- Tab panels -->
            <div class="tab-content">
                <!--Panel 1-->
                <div class="tab-pane fade in show active" id="panel1" role="tabpanel">
                    <div class="row">
                        <div class="col">
                            <h4 class="card-title">Question 1</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <p class="card-text"></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <button type="button" class="btn btn-success float-right">Yes</button>
                        </div>
                        <div class="col">
                            <button type="button" class="btn btn-danger">No</button>
                        </div>    
                    </div>
                </div>
                <!--Panel 2-->
                <div class="tab-pane fade" id="panel2" role="tabpanel">
                    <div class="row">
                        <div class="col">
                            <h4 class="card-title">Question 2</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <p class="card-text"></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <button type="button" class="btn btn-success float-right">Yes</button>
                        </div>
                        <div class="col">
                            <button type="button" class="btn btn-danger">No</button>
                        </div>    
                    </div>
                </div>
                <!--Panel 3-->
                <div class="tab-pane fade" id="panel3" role="tabpanel">
                    <div class="row">
                        <div class="col">
                            <h4 class="card-title">Question 3</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <p class="card-text"></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <button type="button" class="btn btn-success float-right">Yes</button>
                        </div>
                        <div class="col">
                            <button type="button" class="btn btn-danger">No</button>
                        </div>    
                    </div>
                </div>
                <!--Panel 4-->
                <div class="tab-pane fade" id="panel4" role="tabpanel">
                    <div class="row">
                        <div class="col">
                            <h4 class="card-title">Question 4</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <p class="card-text"></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <button type="button" class="btn btn-success float-right">Yes</button>
                        </div>
                        <div class="col">
                            <button type="button" class="btn btn-danger">No</button>
                        </div>    
                    </div>
                </div>
                <!--Panel 5-->
                <div class="tab-pane fade" id="panel5" role="tabpanel">
                    <div class="row">
                        <div class="col"1>
                            <h4 class="card-title">Question 5</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <p class="card-text"></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <button type="button" class="btn btn-success float-right">Yes</button>
                        </div>
                        <div class="col">
                            <button type="button" class="btn btn-danger">No</button>
                        </div>    
                    </div>
                </div>
                <!--Panel 6-->
                <div class="tab-pane fade" id="panel6" role="tabpanel">
                    <div class="row">
                        <div class="col">
                            <h4 class="card-title">Question 6</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <p class="card-text"></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <button type="button" class="btn btn-success float-right">Yes</button>
                        </div>
                        <div class="col">
                            <button type="button" class="btn btn-danger">No</button>
                        </div>    
                    </div>
                </div>
                <!--Panel 7-->
                <div class="tab-pane fade" id="panel7" role="tabpanel">
                    <div class="row">
                        <div class="col">
                            <h4 class="card-title">Question 7</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <p class="card-text"></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <button type="button" class="btn btn-success float-right">Yes</button>
                        </div>
                        <div class="col">
                            <button type="button" class="btn btn-danger">No</button>
                        </div>    
                    </div>
                </div>
                <!--Panel 8-->
                <div class="tab-pane fade" id="panel8" role="tabpanel">
                    <div class="row">
                        <div class="col">
                            <h4 class="card-title">Question 8</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <p class="card-text"></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <button type="button" class="btn btn-success float-right">Yes</button>
                        </div>
                        <div class="col">
                            <button type="button" class="btn btn-danger">No</button>
                        </div>    
                    </div>
                </div>
                <!--Panel 9-->
                <div class="tab-pane fade" id="panel9" role="tabpanel">
                    <div class="row">
                        <div class="col">
                            <h4 class="card-title">Question 9</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <p class="card-text"></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <button type="button" class="btn btn-success float-right">Yes</button>
                        </div>
                        <div class="col">
                            <button type="button" class="btn btn-danger">No</button>
                        </div>    
                    </div>
                </div>
                <!--Panel 10-->
                <div class="tab-pane fade" id="panel10" role="tabpanel">
                    <div class="row">
                        <div class="col">
                            <h4 class="card-title">Question 10</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <p class="card-text"></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <button type="button" class="btn btn-success float-right">Yes</button>
                        </div>
                        <div class="col">
                            <button type="button" class="btn btn-danger">No</button>
                        </div>    
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>

<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/mdb.js"></script>
<script src="js/popper.min.js"></script>

<script>

    let questions
    let question
    let index = 0
    let answers = []

    $(() => {
        loadJSONFromFile('questions', (callback) => {            
            questions = callback            
            setQuestion(1)
            yesButtonDidClicked()
            noButtonDidClicked()
        })
    })

    //

    function loadJSONFromFile(name, callback) {
        $.getJSON(name + '.json', () => {
        }).done((data) => {
            var tempData = []
            $.each(data, (index, item) => {
                tempData.push(item)
            }) 
            callback(tempData)            
        }).fail((_, message, error) => {

        })
    }

    //  DOM Manipulation

    function setQuestion(questionNo) {        
        question = questions.filter((data, _) => { return data.question_no === questionNo })[0]          
        getCurrentPanel().find('.card-title').text('Question ' + getCurrentPanelNumber())      
        getCurrentPanel().find('.card-text').text(question.question_no)     
    }

    function setAnswer(answer) {
        answers.push({ 
            'question_no': getCurrentPanelNumber(),
            'question': question.question,
            'answer': answer
        })

        console.log('Answers: ' + JSON.stringify(answers))

        incrementIndex()

        if(isMoreThan10()) {
            setQuestion(question.answers[answer].next_question_no)
            moveToNextPanel()
        } else {
            let result = {answers: answers, result: question.answers[answer].result } 
            console.log('Result: ' + JSON.stringify(result))            
        }
    }

    function moveToNextPanel() {
        $('#tab').find('a[href="#panel' + getCurrentPanelNumber() + '"]').tab('show')
    }

    function isMoreThan10() {
        return index < 10
    }

    //

    function getCurrentPanel() {        
        return $('#panel' + getCurrentPanelNumber())
    }

    function getCurrentPanelNumber() {
        return index + 1
    }

    //

    function incrementIndex() {
        index += 1
    }
    
    // Actions

    function yesButtonDidClicked() {    
        $('.btn-success') .on('click', () => {
            console.log('Yes Button in question #' + getCurrentPanelNumber() + ' did clicked')
            if(isMoreThan10())
                setAnswer('yes')
        })
    }

    function noButtonDidClicked() {  
        $('.btn-danger') .on('click', () => {
            console.log('No Button in question #' + getCurrentPanelNumber() + ' did clicked')
            if(isMoreThan10())
                setAnswer('no')
        })
    }
</script>
